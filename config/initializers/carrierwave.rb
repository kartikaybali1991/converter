require 'carrierwave/orm/activerecord'

CarrierWave.configure do |config|
  config.root = Rails.root.join('tmp')
  config.cache_dir = 'carrierwave'
  config.fog_directory  = 'directory'
end
