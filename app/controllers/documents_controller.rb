require 'prawn'
require 'yomu'

class DocumentsController < ApplicationController
  def home 
    @document = Document.new
  end

  def index
    @documents = Document.all
  end

  def create
  	@document = Document.new(document_params)
    if @document.save
      yomu = Yomu.new "#{@document.attachment.current_path}"

      if File.extname("#{@document.attachment_identifier}") == '.pdf'
        @document.content = yomu.text
        @document.save
        redirect_to action: 'display'
      else 
        Prawn::Document.generate('test.pdf') do
          font_families.update("Lato" => {
          :normal => Rails.root.join("app/assets/fonts/Lato-Regular.ttf"),
          :italic => Rails.root.join("app/assets/fonts/Lato-Regular.ttf"),
          :bold => Rails.root.join("app/assets/fonts/Lato-Regular.ttf"),
          :bold_italic => Rails.root.join("app/assets/fonts/Lato-Regular.ttf")
        })

        font "Lato"
        text yomu.text
        end

        redirect_to action: 'download'
      end
    end
  end

  def destroy
  	@document = Document.find(params[:id])
  	@document.destroy
  	redirect_to documents_path, notice: "Successfully deleted"
  end

  def download_pdf
    send_file("#{Rails.root}/test.pdf", filename: "test.pdf", type: "application/pdf", disposition: "attachment")
  end

  private   
      def document_params   
      params.require(:document).permit(:attachment, :content) 
   end 
end
