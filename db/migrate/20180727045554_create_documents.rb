class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :attachment
      t.string :content

      t.timestamps null: false
    end
  end
end
